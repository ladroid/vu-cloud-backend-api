import threading
import time, sched
import flask
import logging
import werkzeug

from firebase_admin import credentials, initialize_app, storage

from azure.storage.blob import BlockBlobService

import cloudinary
import cloudinary.uploader
import cloudinary.api

import dropbox

from boxsdk import Client, OAuth2

# upload to firebase
def upload_to_firebase(account_key, storageBucket, filename):
    cred = credentials.Certificate(account_key)
    initialize_app(cred, {'storageBucket':storageBucket})

    bucket = storage.bucket()
    blob = bucket.blob(filename)
    blob.upload_from_filename(filename)
    blob.make_public()
    print('your file url -> ', blob.public_url)
    print('Upload to Firebase successfully!!!')

# delete from firebase
def delete_from_firebase(account_key, storageBucket, filename):
    cred = credentials.Certificate(account_key)
    #initialize_app(cred, {'storageBucket':storageBucket})

    bucket = storage.bucket()
    blob = bucket.blob(filename)
    blob.delete()
    print('Deleted successfully!!!')

# upload to azure
def upload_to_azure(account_name, account_key, container_name, filename):
    dir_name = 'images'

    block_blob_service = BlockBlobService(account_name=account_name, account_key=account_key)
    file_path = '/mnt/e/Projects/VU/Cloud Computing/CloudStorageBackend/src/' + filename
    blob_name = f"{dir_name}/{filename}"
    block_blob_service.create_blob_from_path(container_name, blob_name, file_path)
    print('Uploaded to Azure successfully!!!')

# delete from azure container_name='images/images'
def delete_from_azure(account_name, account_key, container_name, filename):
    block_blob_service = BlockBlobService(account_name=account_name, account_key=account_key)
    block_blob_service.delete_blob(container_name, filename)
    print('Deleted from Azure successfully!!!')

# upload to cloudinary
def upload_to_cloudinary(cloud_name, api_key, api_secret, filename):
    cloudinary.config(cloud_name=cloud_name, api_key=api_key, api_secret=api_secret)
    cloudinary.uploader.upload(filename)
    print('Upload to Cloudinary successfully!!!')

# delete from cloudinary
def delete_from_cloudinary(cloud_name, api_key, api_secret, filename):
    cloudinary.config(cloud_name=cloud_name, api_key=api_key, api_secret=api_secret)
    cloudinary.uploader.destroy(filename)
    print('Deleted from Cloudinary successfully!!!')

# upload to dropbox
def upload_to_dropbox(access_token, filename):
    dbx = dropbox.Dropbox(access_token)
    with open(filename, 'rb') as f:
        dbx.files_upload(f.read(), '/mnt/e/Projects/VU/Cloud Computing/CloudStorageBackend/src/'+filename)
    print('Upload to Dropbox successfully!!!')

# delete from dropbox
def delete_from_dropbox(access_token, filename):
    dbx = dropbox.Dropbox(access_token)
    dbx.files_delete('/mnt/e/Projects/VU/Cloud Computing/CloudStorageBackend/src/'+filename)
    print('Deleted from Dropbox successfully!!!')

# upload to box
def upload_to_box(CLIENT_ID, CLIENT_SECRET, ACCESS_TOKEN, filename):
    oauth2 = OAuth2(CLIENT_ID, CLIENT_SECRET, access_token=ACCESS_TOKEN)
    client = Client(oauth2)
    folder = client.folder(folder_id='0')
    items = folder.get_items()
    for item in items:
        if item.name == filename:
            updated_file = client.file(item.id).update_contents(item.name)
            print('File "{0}" has been updated'.format(updated_file.name))
            return
    folder.upload(filename)
    print('Upload to Box successfully!!!')

# delete from box
def delete_from_box(CLIENT_ID, CLIENT_SECRET, ACCESS_TOKEN, filename):
    '''
    ACCESS_TOKEN: need to be changed each 60 min
    '''
    oauth2 = OAuth2(CLIENT_ID, CLIENT_SECRET, access_token=ACCESS_TOKEN)
    client = Client(oauth2)
    folder = client.folder(folder_id='0')
    items = folder.get_items()
    for item in items:
        if item.name == filename:
            client.file(item.id).delete()
            print('Deleted from Box successfully!!!')
            return
    print('Deleted from Box successfully!!!')

# data sync between firebase and dropbox
def data_sync(account_key, storageBucket, access_token, filename):
    cred = credentials.Certificate(account_key)
    initialize_app(cred, {'storageBucket':storageBucket})

    bucket = storage.bucket()
    blob = bucket.blob(filename)

    fileExists = blob.exists()
    if(fileExists == False):
        delete_from_dropbox(access_token, filename)
    else:
        return

logging.basicConfig(filename='web.log', level=logging.DEBUG, format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

app = flask.Flask(__name__)

s = sched.scheduler(time.time, time.sleep)

#upload_to_box('4f475e55i9blftbl9qxngb2aisun921t', 'Qrec8HmuLa8eiLgEsvmhWnio71lCNMLV', 'OsOyst1WRhHKwRO8JkubYi9TSI1olSDc', 'androidFlask.jpg')
#delete_from_box('4f475e55i9blftbl9qxngb2aisun921t', 'Qrec8HmuLa8eiLgEsvmhWnio71lCNMLV', 'OsOyst1WRhHKwRO8JkubYi9TSI1olSDc', 'androidFlask.jpg')

@app.route('/', methods=['GET', 'POST'])
def handle_event():

    isDelete = flask.request.form.get('delete')
    print(type(isDelete))
    if(isDelete == "true"):
        print('Delete!!!')
        t1 = threading.Thread(target=delete_from_firebase, args=())
        t2 = threading.Thread(target=delete_from_azure, args=())
        t3 = threading.Thread(target=delete_from_cloudinary, args=())
        t4 = threading.Thread(target=delete_from_dropbox, args=())
        t5 = threading.Thread(target=delete_from_box, args=())

        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
    else:
        print('Upload!!!')
        imagefile = flask.request.files['image']
        filename = werkzeug.utils.secure_filename(imagefile.filename)
        print('\nReceived image File name : ', imagefile.filename)
        imagefile.save(filename)

        t1 = threading.Thread(target=upload_to_firebase, args=())
        t2 = threading.Thread(target=upload_to_azure, args=())
        t3 = threading.Thread(target=upload_to_cloudinary, args=())
        t4 = threading.Thread(target=upload_to_dropbox, args=())
        t5 = threading.Thread(target=upload_to_box, args=())

        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
    
    # s.enter(60, 1, data_sync, ('', '', '', filename,))
    # s.run()

    app.logger.info('Info level log')
    app.logger.debug('Debug level log')
    app.logger.warning('Warning level log')
    return 'Upload successfully'

app.run(host='0.0.0.0', port=5000, debug=True)