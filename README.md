# VU-Cloud-Backend-API

<p align="center">
Individual project.
</p>
<p align="center">
Faculty of Mathematics and Informatics.
</p>
<p align="center">
Cloud computing.
</p>
<p align="center">
Developed &copy; by Volodymyr Kadzhaia.
</p> 

## Overview

A backend which use different cloud systems such as Firebase, Azure, Back4App, etc.

## How to use

### Requirements

* Python 3
* Flask
* Firebase SDK
* Azure SDK
* Cloudinary SDK
* Dropbox SDK
* BoxSDK

### Run

```python
python3 start.py
```

